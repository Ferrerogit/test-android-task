package app.popov.testapplication.additional

import android.accounts.NetworkErrorException
import app.popov.testapplication.exception.CustomException
import app.popov.testapplication.exception.InternalServerException
import app.popov.testapplication.exception.NetworkException
import app.popov.testapplication.exception.UnknownException
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import retrofit2.HttpException
import java.net.ConnectException
import java.net.SocketTimeoutException
import java.net.UnknownHostException

fun resolvedLaunch(block: suspend CoroutineScope.() -> Unit,
                   onError: (ex: Exception) -> Unit) {
    GlobalScope.launch(Dispatchers.Main) {
        try {
            block()
        } catch (ex: Exception) {
            ex.printStackTrace()
            when (ex) {
                is HttpException -> {
                    if (ex.code() >= 500)
                        onError(InternalServerException())
                    else
                        onError(ex)
                }
                is CustomException -> onError(ex)
                is UnknownHostException, is NetworkErrorException, is SocketTimeoutException, is ConnectException -> {
                    onError(NetworkException())
                }
                else -> onError(UnknownException())
            }
        }
    }
}
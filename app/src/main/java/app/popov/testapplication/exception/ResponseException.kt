package app.popov.testapplication.exception

class ResponseException(
        var errorMessage: String
) : CustomException()
package app.popov.testapplication

import android.content.Context
import android.util.Log

class Preferences private constructor() {

    companion object {
        private var INSTANCE: Preferences? = null
        fun getInstance(): Preferences {
            val instance = INSTANCE ?: Preferences()
            if (INSTANCE == null)
                INSTANCE = instance
            return instance
        }
    }

    private val APP_PREFERENCES_NAME = "MyTestApp_preferences"

    fun clear(context: Context) {
        val pref = context.getSharedPreferences(APP_PREFERENCES_NAME, Context.MODE_PRIVATE)
        val editor = pref.edit()
        editor.clear()
        editor.apply()
    }


    private fun saveInt(context: Context, key: String, value: Int) {
        val pref = context.getSharedPreferences(APP_PREFERENCES_NAME, Context.MODE_PRIVATE)
        val editor = pref.edit()
        editor.putInt(key, value)
        editor.apply()
    }

    private fun getInt(context: Context, key: String, def: Int = -1): Int {
        val pref = context.getSharedPreferences(APP_PREFERENCES_NAME, Context.MODE_PRIVATE)
        return pref.getInt(key, def)
    }

    private fun saveLong(context: Context, key: String, value: Long) {
        val pref = context.getSharedPreferences(APP_PREFERENCES_NAME, Context.MODE_PRIVATE)
        val editor = pref.edit()
        editor.putLong(key, value)
        editor.apply()
    }

    private fun getLong(context: Context, key: String): Long {
        val pref = context.getSharedPreferences(APP_PREFERENCES_NAME, Context.MODE_PRIVATE)
        return pref.getLong(key, -1)
    }

    private fun saveString(context: Context, key: String, value: String?) {
        val pref = context.getSharedPreferences(APP_PREFERENCES_NAME, Context.MODE_PRIVATE)
        val editor = pref.edit()
        editor.putString(key, value)
        editor.apply()
    }

    private fun getString(context: Context, key: String): String? {
        val pref = context.getSharedPreferences(APP_PREFERENCES_NAME, Context.MODE_PRIVATE)
        return pref.getString(key, null)
    }

    private fun saveBoolean(context: Context, key: String, value: Boolean) {
        val pref = context.getSharedPreferences(APP_PREFERENCES_NAME, Context.MODE_PRIVATE)
        val editor = pref.edit()
        editor.putBoolean(key, value)
        editor.apply()
    }

    private fun getBoolean(context: Context, key: String, default: Boolean): Boolean {
        val pref = context.getSharedPreferences(APP_PREFERENCES_NAME, Context.MODE_PRIVATE)
        val result = pref.getBoolean(key, default)
        Log.d("Preferences", "For $key is $result")
        return result
    }

}